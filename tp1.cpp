#include <iostream>
#include <fstream>
#include <string>     /* self explanatory */
#include <stdio.h>    /* printf, fopen */
#include <stdlib.h>   /* exit , EXIT_FAILURE */
#include <cmath>

#define DICO "words.txt"
#define MOTS_ANG 


// ---------------- //
// -- Prototypes -- //
// ---------------- //

// Methode qui affiche les erreurs (affiche messErr sur stderr)
void affErreur(const char* messErr);

bool estDansDico (char const * mot);

int nombreDeMots();

// ---------------- //
// ----- Main ----- //
// ---------------- //
using std::cout;
using namespace std;


int main(int argc, char * argv[]) 
{
	string nom = DICO;
	bool secondaire = false;
	char c;
	int count=0;
	const int MOTS_EN = nombreDeMots();
	char const *arg1 = argv[1];

	//Lire le fichier
	ifstream fichier( DICO );
	do {
		fichier.get( c );
		cout << c;
		if ( c == '\n')
			++count;
	} while( ! fichier.eof() );
	cout << count <<endl;
	return 0;
}


// --------------- //
// Autres methodes //
// --------------- //

bool estDansDico (char const * mot)
{
	return 0;
}

int nombreDeMots()
{
	int count =0;
	char c;
	ifstream fichier( DICO );
	do {
		fichier.get( c );
		cout << c;
		if ( c == '\n')
			++count;
	} while( ! fichier.eof() );
	return count;
}

void argsValidation(int argc, char * argv[]){
	//Nombre d'arguments
	if (argc > 3)
		affErreur("Le nombre d'argument est trop grand (1 ou 2 sont accepte)");
	else if (argc == 1)
		affErreur("Vous devez minimalement passer un nom de fichier en paramatre");
	//Contenu des arguments

}

void affErreur(const char* messErr) {
	fprintf(stderr, messErr);
	exit(1);
}
